module.exports = async (page, scenario, vp) => {
  await page.goto(scenario.baseUrl+'/user/login');
  page.waitForSelector('#user-login-form');

  const inputName = await page.$('input[name=name]');
  await inputName.type(scenario.email);

  const inputPass = await page.$('input[name=pass]');
  await inputPass.type(scenario.pass);

  const inputSubmit = await page.$('button[name=op]');
  await Promise.all([
    inputSubmit.click(),
    page.waitForNavigation({ waitUntil: 'networkidle0' }),
    page.waitForSelector('.user-logged-in', { timeout: 60000 }),
  ]);
};
