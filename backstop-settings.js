var arguments = require('minimist')(process.argv.slice(2)); // grabs the process arguments
var defaultPaths = ['/']; // By default it just checks the homepage
var scenarios = []; // The array that'll have the pages to test

if (!arguments.testhost) {
  arguments.testhost = "http://localhost"; // Default test host
}

var userConfig = require('./users.js');
for (var i = 0; i < userConfig.array.length; i++) {
  var pathConfig = require('./' + userConfig.array[i].name + '-paths.js');
  var paths = pathConfig.array;
  if (paths.length == 0) {
    paths = defaultPaths; // keep with the default of just the homepage
  }

  for (var j = 0; j < paths.length; j++) {
    var authenticated = {};
    var path = paths[j];
    if (typeof path === 'string' || path instanceof String) {
      delay = 1000;
      url = path;
      onReady = false;
    }
    else {
      delay = path.delay;
      url = path.url;
      onReady = path.onReady;
    }
    if (userConfig.array[i].name != 'anonymous') {
      authenticated = {
        "email": userConfig.array[i].email,
        "pass": userConfig.array[i].pass,
        "onBeforeScript": "puppet/onBeforeAuth.js"
      };
    }

    var anonymous = {
      "baseUrl": arguments.testhost,
      "label": userConfig.array[i].name + ' on ' + url,
      "referenceUrl": arguments.testhost + url,
      "url": arguments.testhost + url,
      "hideSelectors": [],
      "removeSelectors": ['.sliding-popup-bottom'],
      "selectors": [],
      "readyEvent": null,
      "delay": delay,
      "postInteractionWait": 0,
      "misMatchThreshold": 1.5,
      "onBeforeScript": "puppet/onBefore.js"
    };
    if (onReady) {
      anonymous['onReadyScript'] = onReady;
    }
    
    // Merge obj if running for authenticated user.
    if (Object.keys(authenticated).length > 0) {
      scenarios.push(Object.assign(anonymous, authenticated));
    }
    else {
      scenarios.push(anonymous);
    }
  }
  pathConfig.length = 0;
  paths.length = 0;
}

module.exports =
    {
      "id": "Visual Regression Testing",
      "viewports": [
        {
          "label": "Phone",
          "width": 375,
          "height": 667
        },
        {
          "name": "Tablet",
          "width": 768,
          "height": 1024
        },
        {
          "name": "Desktop large",
          "width": 1440,
          "height": 900
        }
      ],
      "scenarios":
        scenarios,
      "paths": {
        "bitmaps_reference": "backstop_data/bitmaps_reference",
        "bitmaps_test": "backstop_data/bitmaps_test",
        "html_report": "backstop_data/html_report",
        "ci_report": "backstop_data/ci_report"
      },
      "engine": "puppeteer",
      "engineOptions": {
        "ignoreHTTPSErrors": true,
        "slowMo": 0,
        "headless": true,
        "args": [
          "--no-sandbox",
          "--disable-setuid-sandbox",
          "--disable-gpu",
          "--force-device-scale-factor=1",
          "--disable-infobars",
          "--hide-scrollbars",
          "--disable-dev-tools",
          "--disable-extensions",
          "--disable-extensions-file-access-check",
          "--disable-login-animations",
          "--disable-speech-api",
          "--disable-suggestions-ui",
          "--disable-voice-input",
          "--disable-print-preview",
          "--disable-search-geolocation-disclosure",
          "--disable-smooth-scrolling",
          "--disable-sync",
          "--disable-third-party-keyboard-workaround",
          "--disable-web-security",
          "--enable-fast-unload",
          "--mute-audio",
          "--disable-default-apps",
          "--ignore-certificate-errors",
          "--proxy-server='direct://",
          "--proxy-bypass-list=*",
        ]
      },
      "report": ["CLI"],
      "asyncCaptureLimit": 8,
      "asyncCompareLimit": 40,
      "debugWindow": false,
      "debug": false
    };