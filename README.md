## Local development
To run the tests locally, you need to download the latest version of the tests
scripts from https://git.1xinternet.de/1xInternet/drupal-vr-testing-backstopjs/-/releases

You should then have a file called drupal-vr-testing-backstopjs-0.x with an
archive extension.

Extract that archive inside the `tests/backstopjs` directory in the project. If
this directory doesn't exist create one.

Once the archive is extracted, run the following commands:

```
ln -s onBeforeAuth-d8.js backstop_data/engine_scripts/puppet/onBeforeAuth.js
yarn
```

Then add the files described in the next steps, and you can now run the tests,
starting with the visual-reference:

```
./node_modules/.bin/backstop reference --configPath=backstop-settings.js --testhost=http://dev.vm${vm_number}.hosting.1xinternet.de
```

and after applying the changes:
```
./node_modules/.bin/backstop test --configPath=backstop-settings.js --testhost=http://dev.vm${vm_number}.hosting.1xinternet.de
```

If you're not using the dev VM, replace the testhost URL above with your local
development environment.

## Configure the VR tests

The tests are configured by the following files in the `tests/backstopjs`
directory in your site's repository. If these files don't yet exist they have to
be created. You can use the examples below as a template.

* `.gitignore`
* `users.js`
* `[user]-paths.js`

You need to declare a `[user]-paths.js` for each user declared in `users.js`.
If you declare the administrator and anonymous users you need to provide the
`administrator-paths.js` and `anonymous-paths.js`.

### users.js
```
var userConfig = {};

userConfig.array = [
  {name:'username', email:'email', pass:'pass'},
  {name:'anonymous'}
]

module.exports = userConfig;
```

where username is something like 'administrator', 'editor', 'anonymous' etc.

### [user]-paths.js
```
var pathConfig = {};

pathConfig.array = [
  '/',
  '/path1',
  '/path2/path3',
]

module.exports = pathConfig;
```

Each path should start with '/', and you can provide as many paths as you want.

### .gitignore
```
backstop-settings.js
backstop.json
backstop_data
package.json

node_modules
yarn.lock
```

It is recommended to add this `.gitignore` file to avoid committing the common
VR test files by mistake.

## Configure the CI to run the VR tests

The common Gitlab CI (/1xInternet/gitlab-ci-common/) includes a step to deploy
and execute these test tools, assuming they are located in `tests/backstopjs`.

To configure it, the following needs to be added to the site's .gitlab-ci.ymkl file:
```
stages:
  - build
  - pre_testing
  - deploy
  - testing

include:
  - project: '1xInternet/gitlab-ci-common'
    file: 'gitlab-ci-d8.yml'

VR_reference_on_stage:
  stage: pre_testing
  extends: .drupal8_vr_reference
  variables:
    env: stage
  only:
    - release

VR_tests_on_stage:
  stage: testing
  extends: .drupal8_vr_test
  variables:
    env: stage
    vr_emails: f.last@1xinternet.de
  only:
    - release
```

You'll probably need to add the pre_testing and testing stages, if not already
present, and then configure the branches where VR testing should be done.
'env' is set to stage by default, but if you're using the develop branch,
change the env variable to 'test' in both steps.
